# AutoAgora

:warning: **Beware! This software is still experimental, use at your own risk!**
:warning:

An [Agora](https://github.com/graphprotocol/agora) cost model automation tool for The
Graph indexers:

- Automates the creation of relative prices for commonly seen query skeletons.
  (Off by default).
- Continously tunes a per-subgraph absolute price multiplier, using continuous
  online reinforcement learning.
