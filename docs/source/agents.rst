Agents API (Python)
===================

Agent
-----
.. autoclass:: price_multiplier_bandit.agent.Agent
   :members:
   :undoc-members:
   :show-inheritance:


ContinuousActionBandit
----------------------
.. autoclass:: price_multiplier_bandit.price_bandit.ContinuousActionBandit
   :members:
   :undoc-members:
   :show-inheritance:


VanillaPolicyGradientBandit
---------------------------
.. autoclass:: price_multiplier_bandit.price_bandit.VanillaPolicyGradientBandit
   :members:
   :undoc-members:
   :show-inheritance:


ProximalPolicyOptimizationBandit
--------------------------------
.. autoclass:: price_multiplier_bandit.price_bandit.ProximalPolicyOptimizationBandit
   :members:
   :undoc-members:
   :show-inheritance:


RollingMemContinuousBandit
--------------------------
.. autoclass:: price_multiplier_bandit.price_bandit.RollingMemContinuousBandit
   :members:
   :undoc-members:
   :show-inheritance:
