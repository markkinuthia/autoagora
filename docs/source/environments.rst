Environments API (Python)
=========================


Environment
-----------
.. autoclass:: price_multiplier_bandit.environment.Environment
   :members:
   :undoc-members:
   :show-inheritance:



SimulatedSubgraph
-----------------
.. autoclass:: price_multiplier_bandit.simulated_subgraph.SimulatedSubgraph
   :members:
   :undoc-members:
   :show-inheritance:


NoisyQueriesSubgraph
--------------------
.. autoclass:: price_multiplier_bandit.simulated_subgraph.NoisyQueriesSubgraph
   :members:
   :undoc-members:
   :show-inheritance:


NoisyCyclicQueriesSubgraph
--------------------------
.. autoclass:: price_multiplier_bandit.simulated_subgraph.NoisyCyclicQueriesSubgraph
   :members:
   :undoc-members:
   :show-inheritance:
